package vehicle;

import vehicle.Vehicle;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Bicycle extends Vehicle {
    private int gears;
    private Calendar productionDate;

    public Bicycle(){

    }

    public Bicycle(String name, String colour, int price, int model, String serialNumber, int gears, int direction){
        setName(name);
        setColour(colour);
        setPrice(price);
        setModel(model);
        setSerialNumber(serialNumber);
        setDirection(direction);
        setGears(gears);
        setProductionDate(new GregorianCalendar());
    }

    public Bicycle(String name, String colour, int price, int model, String serialNumber, int gears){
        setName(name);
        setColour(colour);
        setPrice(price);
        setModel(model);
        setSerialNumber(serialNumber);
        setDirection(0);
        setGears(gears);
        setProductionDate(new GregorianCalendar());
    }

    @Override
    public void setAllFields() {
        super.setAllFields();
    }

    @Override
    public void turnLeft(int degrees) {
        System.out.println("Bicycle turned " + (-degrees) + " degrees");
    }

    @Override
    public void turnRight(int degrees) {
        System.out.println("Bicycle turned " + degrees + " degrees");
    }

    public int getGears() {
        return gears;
    }

    public Calendar getProductionDate() {
        return productionDate;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public void setProductionDate(Calendar productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public String toString() {
        return "name: '" + getName() + '\'' + '\n' +
                "colour: '" + getColour() + '\'' + '\n' +
                "serialnumber=: '" + getSerialNumber() + '\'' + '\n' +
                "model: '" +getModel() + '\'' + '\n' +
                "price: '" + getPrice() + '\'' +  '\n' +
                "direction: '" + getDirection() + '\'' + '\n' +
                "speed: '" + getSpeed() +'\'' +  '\n' +
                "gears: '" + gears + '\'' + '\n' +
                "production date: " + productionDate.get(Calendar.YEAR) + '-' + productionDate.get(Calendar.MONTH) + '-' + productionDate.get(Calendar.DAY_OF_MONTH)  + '\n';
    }

}
