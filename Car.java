package vehicle;

import vehicle.Vehicle;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Car extends Vehicle {
    private int power;
    private Calendar productionDate;

    public Car(){

    }

    public Car(String name, String colour, int price, int model, String serialNumber, int power, int direction){
        setName(name);
        setColour(colour);
        setPrice(price);
        setModel(model);
        setSerialNumber(serialNumber);
        setDirection(direction);
        setPower(power);
        setProductionDate(new GregorianCalendar());
    }

    public Car(String name, String colour, int price, int model, String serialNumber, int power){
        setName(name);
        setColour(colour);
        setPrice(price);
        setModel(model);
        setSerialNumber(serialNumber);
        setDirection(0);
        setPower(power);
        setProductionDate(new GregorianCalendar());
    }

    @Override
    public void setAllFields() {
        super.setAllFields();
    }

    @Override
    public void turnRight(int degrees) {
        if (degrees >= 0 && degrees <= 360){
            setDirection(getDirection() + degrees);
            if (getDirection() == 360 || getDirection() == 0) {
                setDirection(0);
            }
            else if (getDirection() > 360){
                setDirection(getDirection() - 360);
            }
            else if (getDirection() < 0){
                setDirection(getDirection() + 360);
            }
        }
    }

    @Override
    public void turnLeft(int degrees) {
        if (degrees >= 0 && degrees <= 360){
            setDirection(getDirection() - degrees + 360);
            if (getDirection() == 360 || getDirection() == 0) {
                setDirection(0);
            }
            else if (getDirection() > 360){
                setDirection(getDirection() - 360);
            }
            else if (getDirection() < 0){
                setDirection(getDirection() + 360);
            }
        }
    }

    public int getPower() {
        return power;
    }

    public Calendar getProductionDate() {
        return productionDate;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setProductionDate(Calendar productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public String toString() {
        return "name: '" + getName() + '\'' + '\n' +
                "colour: '" + getColour() + '\'' + '\n' +
                "serialnumber=: '" + getSerialNumber() + '\'' + '\n' +
                "model: '" +getModel() + '\'' + '\n' +
                "price: '" + getPrice() + '\'' +  '\n' +
                "direction: '" + getDirection() + '\'' + '\n' +
                "speed: '" + getSpeed() +'\'' +  '\n' +
                "power: '" + power + '\'' + '\n' +
                "production date: " + productionDate.get(Calendar.YEAR) + '-' + productionDate.get(Calendar.MONTH) + '-' + productionDate.get(Calendar.DAY_OF_MONTH)  + '\n';
    }
}
