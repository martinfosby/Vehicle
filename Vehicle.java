package vehicle;

import java.util.Scanner;

public abstract class Vehicle {
    private String colour, name, serialNumber;
    private int model, price, direction;
    private double speed = 0;
    protected java.util.Scanner input;

    public Vehicle(){

    }

    public Vehicle(String name, String colour, int price, int model, String serialNumber, int direction){
        this.colour = colour;
        this.name = name;
        this.serialNumber = serialNumber;
        this.model = model;
        this.price = price;
        this.direction = direction;
    }

    public void setAllFields() {
        input = new Scanner(System.in);

        System.out.println("Enter the colour you fecking dick");
        colour = input.nextLine();
        System.out.println("Enter the name you fecking dick");
        name = input.nextLine();
        System.out.println("Enter the serialNumber you fecking dick");
        serialNumber = input.nextLine();
        System.out.println("Enter the model you fecking dick");
        model = input.nextInt();
        System.out.println("And finally enter the fecking price you fecking dick");
        price = input.nextInt();
    }

    public abstract void turnLeft(int degrees);
    public abstract void turnRight(int degrees);

    public String getName() {
        return name;
    }

    public String getColour() {
        return colour;
    }

    public int getPrice() {
        return price;
    }

    public int getModel() {
        return model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public int getDirection() {
        return direction;
    }

    public double getSpeed() {
        return speed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "name: '" + name + '\'' + '\n' +
                "colour: '" + colour + '\'' + '\n' +
                "serialnumber=: '" + serialNumber + '\'' + '\n' +
                "model: '" + model + '\'' + '\n' +
                "price: '" + price +  '\'' + '\n' +
                "direction: '" + direction + '\'' + '\n' +
                "speed: '" + speed + '\'' + '\n' +
                "input: '" + input + '\'' + '\n';
    }
}
