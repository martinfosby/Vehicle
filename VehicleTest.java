package vehicle;


/**
 * TestVehicles oppretter Bicycle og Car objekter, legger disse i et ArrayList
 * Lar bruker manipulere data i arrayet på forskjellige måter
 */

import java.util.*;
import java.io.*;

public class VehicleTest {
  public static void main(String[] args) {
	  VehicleTest vtest = new VehicleTest();
    try {
      vtest.menuLoop();
    } catch(IOException e) {
      System.out.println("IO Exception!");
      System.exit(1);
    } catch(CloneNotSupportedException e) {
      System.out.println("CloneNotSupportedException");
      System.exit(1);
    }
  }

  private void menuLoop() throws IOException, CloneNotSupportedException {
    Scanner scan = new Scanner(System.in);
    ArrayList<Vehicle> arr = new ArrayList<Vehicle>();
    Vehicle vehicle;

    arr.add(new Car("Volvo","Black",85000,2010,"1010-11",163,0));
    arr.add(new Bicycle("Diamant","yellow",4000,1993,"BC100",10,0));
    arr.add(new Car("Ferrari Testarossa","red",1200000,1996,"A112",350,0));
    arr.add(new Bicycle("DBS","pink",5000,1994,"42",10,0));

    while(true) {
      System.out.println("1...................................New car");
      System.out.println("2...............................New bicycle");
      System.out.println("3......................Find vehicle by name");
      System.out.println("4..............Show data about all vehicles");
      System.out.println("5.......Change direction of a given vehicle");
      System.out.println("6..............................Exit program");
      System.out.println(".............................Your choice?");
      int choice = scan.nextInt();

      Scanner scanner = new Scanner(System.in);
      String name;
      String colour;
      int price;
      int model;
      String serialNumber;

      switch (choice) {
      case 1:
        //legg til en ny bil
        int power;

        System.out.println("Input car data:");
        System.out.printf("Name: ");
        name = scanner.nextLine();
        System.out.printf("Colour: ");
        colour = scanner.nextLine();
        System.out.printf("Price: ");
        price = scanner.nextInt();
        System.out.printf("Model: ");
        model = scanner.nextInt();
        scanner.nextLine();
        System.out.printf("Serial: ");
        serialNumber = scanner.nextLine();
        System.out.printf("Power: ");
        power = scanner.nextInt();

        arr.add(new Car(name, colour, price, model, serialNumber, power));
        break;
      case 2:
        //legg til en ny sykkel
        int gears;

        System.out.println("Input bicycle data:");
        System.out.printf("Name: ");
        name = scanner.nextLine();
        System.out.printf("Colour: ");
        colour = scanner.nextLine();
        System.out.printf("Price: ");
        price = scanner.nextInt();
        System.out.printf("Model: ");
        model = scanner.nextInt();
        System.out.printf("Serial: ");
        serialNumber = scanner.nextLine();
        System.out.printf("Gears: ");
        gears = scanner.nextInt();

        arr.add(new Bicycle(name, colour, price, model, serialNumber, gears));
        break;
      case 3:
        //vis info om gitt kjøretøy
        System.out.printf("Name of vechicle: ");
        name = scanner.nextLine();
        for (int i = 0; i < arr.size(); i++) {
          if (arr.get(i).getName().equals(name)){
            System.out.println(arr.get(i).toString());
          }
        }
        break;
      case 4:
        //vis info om alle kjøretøy
        for (int i = 0; i < arr.size(); i++) {
          System.out.println(arr.get(i).toString());
        }
        break;
      case 5:
        // Finn kjøretøy med gitt navn, sett ny retning
        System.out.printf("Name of vehicle: ");
        name = scanner.nextLine();
        System.out.printf("Direction [R/L]: ");
        String direction = scanner.nextLine();
        direction.toLowerCase();
        System.out.printf("Degrees [0-360]: ");
        int degrees = scanner.nextInt();
        for (int i = 0; i < arr.size(); i++) {
          if (arr.get(i).getName().equals(name)){
            if (direction.equals("r")){
              arr.get(i).turnRight(degrees);
            }
            if (direction.equals("l")){
              arr.get(i).turnLeft(degrees);
            }
          }
        }
        break;
      case 6:
      	scan.close();
        System.exit(0);
      default:
        System.out.println("Wrong input!");
      }
    }
  }
}

